/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos_UI;

import gestoreventos.Candidatura;
import gestoreventos.CentroEventos;
import gestoreventos.Evento;
import gestoreventos.Participante;
import gestoreventos.RepParticipante;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class SubmeteCandidatura_UI {

    private CentroEventos centroEventos;
    private Evento evento;

    /**
     * Constroi uma instância de DecideCandidatura_UI recebendo por parâmetro
     * centroeventos.
     *
     * @param centroEventos CentroEventos que possui todos os dados
     */
    public SubmeteCandidatura_UI(CentroEventos centroEventos) {
        this.centroEventos = centroEventos;

    }
    /**
     * cria uma instância de Scanner
     */
    private static final Scanner in = new Scanner(System.in);

    /**
     * Método que mostra as opções para Submeter candidatura a um evento e
     * submete uma candidatura
     */
    public void run() {

        List<Evento> eventosSubmossiveis = centroEventos.getEventosSubmissiveis();
        do {
            List<Evento> eventosSumbissiveis = centroEventos.criaEventosSubmissiveis();
            if (eventosSumbissiveis.isEmpty()) {
                System.out.println("Não existem eventos em fase de submissão de candidaturas");
            } else {
                System.out.println();
                System.out.println("Seleccione, um Evento da Lista de Eventos em fase de submisao de candidaturas: " + centroEventos.apresentaInfoEventosSubmissiveis(centroEventos.criaEventosSubmissiveis()));
                System.out.println("insira o título do evento: ");
                String titulo = in.nextLine().toLowerCase();
//                in.nextLine();
                evento = centroEventos.seleccionarEventoSubmissivel(titulo);
            }
        } while (evento == null);

        System.out.println();
        System.out.println("qual o username do Representante do Participante?"+centroEventos.getListaRepParticipantes());
        String username = in.nextLine();
//        in.nextLine();
        RepParticipante repParticipante = this.centroEventos.getRepParticipantePorUsername(username);

        Candidatura candidatura = new Candidatura(repParticipante, evento);
        evento.adicionaCandidatura(candidatura);

    }

}
