package gestoreventos_UI;

import gestoreventos.CentroEventos;
import gestoreventos.Evento;
import gestoreventos.FAE;
import gestoreventos.Organizador;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class Janela_UI {

    /**
     * CentroEventos que contém o registo de todo
     */
    private CentroEventos centroEventos;

    /**
     * Guarda a opção escolhida pelo utilizador do sistema
     */
    private String opcao;
    private Organizador faes;
    private FAE candidaturasParaDecidir;
    private Evento evento;

    /**
     * Constroi uma instância de JanelaU_I recebendo por parâmetro
     * centroeventos.
     *
     * @param centroEventos CentroEventos que possui todos os dados
     */
    public Janela_UI(CentroEventos centroEventos) {
        this.centroEventos = centroEventos;
    }

    /**
     *cria uma instância de scanner
     */
    private static final Scanner in = new Scanner(System.in);

    /**
     * Método que mostra as funcionalidaddes que a aplicação tem e permite
     * interagir com elas
     *
     */
    public void run() {
        do {
            System.out.println();
            System.out.println("3. Atribuir Candidatura aos FAEs");
            System.out.println("4. Decidir Candidatura");
            System.out.println("5. Submeter Candidatura");
            System.out.println("0. Sair");
            opcao = readLineFromConsole("Introduza opção: ");
//            in.nextLine();
            switch (opcao) {
                case "3":
                    AtribuiCandidatura_UI atribuiCandidatura_ui = new AtribuiCandidatura_UI(centroEventos);
                    atribuiCandidatura_ui.run();
                    break;
                case "4":
                    DecideCandidatura_UI decideCandidatura_ui = new DecideCandidatura_UI(centroEventos);
                    decideCandidatura_ui.run();
                    break;
                case "5":                    
                    SubmeteCandidatura_UI submeterCandidatura_ui = new SubmeteCandidatura_UI(centroEventos);
                    submeterCandidatura_ui.run();
            }
        } while (!opcao.equals("0"));

    }
    /**
     * método que lê uma string introduzida pelo utilizador na consola
     *
     * @param strPrompt - string apresentada
     * @return string introduzida pelo utilizador
     */
    public static String readLineFromConsole(String strPrompt) {
        try {
            System.out.println();
            System.out.println(strPrompt);

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            return in.readLine();
        } catch (Exception e) {
            return null;
        }
    }

}
