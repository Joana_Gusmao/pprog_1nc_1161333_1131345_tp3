package gestoreventos_UI;


import gestoreventos.*;
import gestoreventos_utils.*;
import gestoreventos_UI.*;
import java.util.ArrayList;
import java.util.List;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class Main {

    public static void main(String[] args) {
        LerFicheiroTexto leitorTexto = new LerFicheiroTexto();
           
        CentroEventos centroEventos = new CentroEventos();
           centroEventos.setEventos(leitorTexto.getEventos());
        centroEventos.setFaes(leitorTexto.getFaes());
        centroEventos.setListaOrganizadores(leitorTexto.getOrganizadores());
        centroEventos.setListaRepParticipantes(leitorTexto.getRepsParticipantes());

        Janela_UI janela_ui = new Janela_UI(centroEventos);
        janela_ui.run();

    }
}
