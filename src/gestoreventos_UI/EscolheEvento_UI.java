/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos_UI;

import gestoreventos.CentroEventos;
import gestoreventos.Evento;
import java.util.Scanner;

/**
 *
 * @author joanagusmaoguedes
 */
public class EscolheEvento_UI {

    private CentroEventos centroEventos;
    private Evento evento;

    /**
     * Constroi uma instância de AtribuiCandidatura_UI recebendo por parâmetro
     * centroeventos.
     *
     * @param centroEventos CentroEventos que possui todos os dados
     */
    public EscolheEvento_UI(CentroEventos centroEventos) {
        this.centroEventos = centroEventos;
    }

    /**
     * Cria uma instância de Scanner
     */
    private static final Scanner in = new Scanner(System.in);

    public Evento run() {

        String tituloEvento;
        do {
            System.out.println("lista de eventos: \n"+centroEventos.getEventos());
           centroEventos.criaEventosSubmissiveis();
            System.out.println("\nLista de eventos submissiveis: \n" +centroEventos.getEventosSubmissiveis());
            System.out.println("\n\n Qual o título do evento que pretende seleccionar?");
            tituloEvento = in.nextLine();
            in.nextLine();
            System.out.println();
           return centroEventos.seleccionarEventoPorTitulo(tituloEvento);
        } while (tituloEvento == null);
    }

}
