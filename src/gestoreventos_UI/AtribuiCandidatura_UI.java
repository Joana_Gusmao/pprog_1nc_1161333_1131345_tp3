/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos_UI;

import gestoreventos.CentroEventos;
import gestoreventos.Decisao;
import gestoreventos.DecisaoFAEsComMenorCargaDeTrabalho;
import gestoreventos.DecisaoPorExperienciaFAE;
import gestoreventos.DecisaoPorNrFAE;
import gestoreventos.Evento;
import gestoreventos.FAE;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Jorge Mota
 */
public class AtribuiCandidatura_UI {

    private CentroEventos centroEventos;
    private Evento evento;

    /**
     * Constroi uma instância de AtribuiCandidatura_UI recebendo por parâmetro
     * centroeventos.
     *
     * @param centroEventos CentroEventos que possui todos os dados
     */
    public AtribuiCandidatura_UI(CentroEventos centroEventos) {
        this.centroEventos = centroEventos;
    }

    /**
     * Cria uma instância de Scanner
     */
    private static final Scanner in = new Scanner(System.in);

    /**
     * método que mostra as opções para atribuição de candidatura aos FAEs e
     * atribui as candidaturas aos FAEs escolhidos
     */
    public void run() {
        int escolha = 0;
        Decisao decisao = null;
        String algoritmo = "";
        EscolheEvento_UI escolheEvento = new EscolheEvento_UI(centroEventos);
        evento = escolheEvento.run();
        do {
            int algoritmoDeDecisao = algoritmoDeDecisao();
            switch (algoritmoDeDecisao) {
                case 1:
                    decisao = new DecisaoFAEsComMenorCargaDeTrabalho();
                    evento.setFaesEscolhidos(decisao.EscolherFAEs(evento, centroEventos, null));
                    algoritmo = "menor carga de trabalho";
                    break;
                case 2:
                    do {
                        int parametro = grauExperienciaFAE();
                        decisao = new DecisaoPorExperienciaFAE();
                        evento.setFaesEscolhidos(decisao.EscolherFAEs(evento, centroEventos, parametro));
                        algoritmo = "grau de experiência em organização de eventos";
                        if (evento.getFaesEscolhidos().isEmpty()) {
                            System.out.println("Não existem FAEs com o grau de experiência seleccionado. Tente outro inteiro entre 1 e 3 além de " + parametro + ".");
                        }
                    } while (evento.getFaesEscolhidos().isEmpty());
                    break;
                case 3:
                    System.out.println("Quandos FAEs são necessários para o evento?");
                    evento.setnFAE(in.nextInt());
                    decisao = new DecisaoPorNrFAE();
                    evento.setFaesEscolhidos(decisao.EscolherFAEs(evento, centroEventos, null));
                    algoritmo = "número de FAEs necessários, escolhidos aleatoriamente";
                    break;
            }

            System.out.println();
            System.out.println("\nA lista de FAEs escolhidos pelo algoritmo '" + algoritmo + "' é:\n" + evento.getFaesEscolhidos());
            System.out.println();
            System.out.println("\nSeleccionou o algoritmo definitivo?\n "
                    + "0. Não\n"
                    + "1. Sim\n");
            escolha = in.nextInt();
            in.nextLine();
        } while (escolha == 0);
        List<FAE> faesEscolhidos = evento.getFaesEscolhidos();
//        Distribuicao distribuicao = new Distribuicao(evento, faesEscolhidos);
        evento.distribuiCandidaturasEActualizaCargaTrabalhoEExperiencia();
        System.out.println(evento.getFaesEscolhidos());
    }

    /**
     * Menu de seleccão apresentado em consola ao utilizador de forma a que
     * seleccione o tipo de algoritmo de decisão que escolheu
     *
     * @return
     */
    public static int algoritmoDeDecisao() {

        System.out.println();
        System.out.println("\nComo pretende escolher os o(s) FAE(s)?\n"
                + "     1. Por menor carga de trabalho \n"
                + "     2. Por grau de experiência em organização de eventos \n"
                + "     3. Por número de FAEs necessários, escolhidos de forma aleatória \n");
        int algoritmoEscolhido = in.nextInt();
        in.nextLine();
        return algoritmoEscolhido;
    }

    /**
     * método para receber input do utilizador acerca do grau de experiência dos
     * FAEs que pretende escolher
     *
     * @return int experiência
     */
    public static int grauExperienciaFAE() {
        int parametro;
        do {
            System.out.println();
            System.out.println("Qual o grau de experiência que pretende seleccionar? Introduza um inteiro entre 1 e 3.");
            parametro = in.nextInt();
            in.nextLine();
        } while (parametro < 1 || parametro > 3);
        return parametro;
    }
    
   

}
