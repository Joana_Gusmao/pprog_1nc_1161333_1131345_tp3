/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos_UI;

import gestoreventos.Avaliacao;
import gestoreventos.Candidatura;
import gestoreventos.CentroEventos;
import gestoreventos.Evento;
import gestoreventos.FAE;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author Jorge Mota
 */
public class DecideCandidatura_UI {

    private String tituloEvento;
    private String usernameRepParticipante;
    private String opcao;
    private String texto;
    private CentroEventos centroEventos;
    private Evento evento;
    private Candidatura candidatura;
    private Avaliacao avaliacao = new Avaliacao();
    private FAE fae;

    /**
     * Constroi uma instância de DecideCandidatura_UI recebendo por parâmetro
     * centroeventos.
     *
     * @param centroEventos CentroEventos que possui todos os dados
     */
    public DecideCandidatura_UI(CentroEventos centroEventos) {
        this.centroEventos = centroEventos;
    }
    /**
     * Cria uma instância de Scanner
     */
    private static final Scanner in = new Scanner(System.in);

    /**
     * Método que mostra as opções para Decidir candidatura a um evento e decide
     * uma candidatura
     */
    public void run() {
//        EscolheEvento_UI escolheEvento = new EscolheEvento_UI(centroEventos);
//        evento = escolheEvento.run();
        System.out.println();
        System.out.println("Qual o seu username?");
        String username = in.nextLine();
        fae = centroEventos.getFAEPorUsername(username);
        System.out.println("Lista de candidaturas:"+ fae.getCandidaturasParaDecidir().toString());
        System.out.println("Qual o nome do evento cuja candidatura pretende decidir?");
        tituloEvento = in.nextLine();
        in.nextLine();
        System.out.println();
        System.out.println("Qual o username dp Representante do Participante?");
        usernameRepParticipante = in.nextLine();
        in.nextLine();
        candidatura = centroEventos.seleccionarCandidaturaPorTituloEventoERepParticipante(tituloEvento, usernameRepParticipante);
        do {

            System.out.println();
            System.out.println("1. Aprovar candidatura");
            System.out.println("2. Rejeitar candidatura");
            opcao = readLineFromConsole("Qual a decisão acerca da candidatura?");
            in.nextLine();

            switch (opcao) {
                case "1":
                    avaliacao.setAceite(true);
                    break;
                case "2":
                    avaliacao.setAceite(false);
                    break;
            }
        } while (!opcao.equals("0") && !opcao.equals("1"));
//        candidatura.setAvaliacao(avaliacao);
        do {
            System.out.println();
            System.out.println("Escreva o texto justificativo da sua decisão");
            texto = in.nextLine();
            in.nextLine();
            avaliacao.setJustificação(texto);
        } while (texto.isEmpty());
        candidatura.setAvaliacao(avaliacao);
        fae.diminuirCargaTrabalho();

    }

    /**
     * método que lê uma string introduzida pelo utilizador na consola
     *
     * @param strPrompt - string apresentada
     * @return string introduzida pelo utilizador
     */
    public static String readLineFromConsole(String strPrompt) {
        try {
            System.out.println();
            System.out.println(strPrompt);

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            return in.readLine();
        } catch (Exception e) {
            return null;
        }
    }
}
