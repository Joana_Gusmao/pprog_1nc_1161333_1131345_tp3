package gestoreventos_utils;

import gestoreventos.CentroEventos;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class LerGravarFicheiro{

    /**
     * Nome do ficheiro onde se vão guardar os dados que estão na empresa
     */
    public static final String NOME_FICHEIRO = "Dados.bin";

    /**
     * Método que contrói uma intância de FicheiroDados sem paramêtros
     */
    public LerGravarFicheiro(){
    }

    /**
     * Método que retorna um objeto CentroEventos guardado num ficheiro binário com
     * todos os dados que o constituem
     *
     * @param nomeFicheiro - Nome do ficheiro binário
     * @return - CentroEventos
     */
    public static CentroEventos ler(String NOME_FICHEIRO){
        CentroEventos centroEventos;
        
        try(FileInputStream fi = new FileInputStream(NOME_FICHEIRO)){
        
            ObjectInputStream os = new ObjectInputStream(fi);
            try
            {
                centroEventos = (CentroEventos) os.readObject();
            } finally
            {
                os.close();
            }
            return centroEventos;
        } catch (IOException | ClassNotFoundException ex)
        {
            return null;
        }
    }

    /**
     * Método que guarda num ficheiro binário todos os dados referentes ao
     * objecto centroeventos, lança exceção caso não consiga caso não consiga 
     * escrever para o ficheiro
     *
     * @param NOME_FICHEIRO  - Nome do ficheiro
     * @param centroEventos - Objeto a guardar
     * @return - True se foi guardado com sucesso
     */
    public boolean guardar(String NOME_FICHEIRO, CentroEventos centroEventos)
    {
        try(FileOutputStream fs = new FileOutputStream(NOME_FICHEIRO)){
            
            ObjectOutputStream out = new ObjectOutputStream(fs);
            try
            {
                out.writeObject(centroEventos);
            } finally
            {
                out.close();
            }
            return true;
        } catch (IOException ex)
        {
            return false;
        }
    }
}
