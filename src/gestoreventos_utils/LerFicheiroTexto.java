package gestoreventos_utils;

import gestoreventos.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class LerFicheiroTexto {

    private CentroEventos centroEventos;
    /**
     * Nome do /ficheiro que contém os dados dos objetos para testar
     */
    private static final String FILE_INPUT = "input.txt";
    //private gestoreventos.Participante listaParticipantes;

    private List<GestorEventos> gestores = new ArrayList<>();
    private List<FAE> faes = new ArrayList<>();
    private List<Participante> participantes = new ArrayList<>();
    private List<RepParticipante> repsParticipantes = new ArrayList<>();
    private List<Organizador> organizadores = new ArrayList<>();
    private List<Evento> eventos = new ArrayList<>();
    private List<Candidatura> candidaturas = new ArrayList<>();

    public List<GestorEventos> getGestores() {
        return gestores;
    }

    public List<FAE> getFaes() {
        return faes;
    }

    public List<Participante> getParticipantes() {
        return participantes;
    }

    public List<RepParticipante> getRepsParticipantes() {
        return repsParticipantes;
    }

    public List<Organizador> getOrganizadores() {
        return organizadores;
    }

    public List<Evento> getEventos() {
        return eventos;
    }

    public List<Candidatura> getCandidaturas() {
        return candidaturas;
    }

    
    
    /**
     * Constroi uma instância de Ficheiro recebendo um centroeventos.
     *
     * @param centroEventos
     */
    public LerFicheiroTexto(CentroEventos centroEventos) {
        this.centroEventos = centroEventos;
    }

    /**
     * Método que lê os dados do ficheiro de testes e instancia os objetos com
     * os dados que este possui, separando o conteúdo de cada linha pelo ";".
     * Utiliza a Classe FileReader para ler o ficheiro, e BufferedReader para
     * armazenar o conteúdo de cada linha
     */
    public LerFicheiroTexto() {
        try {
            FileReader ficheiro = new FileReader(FILE_INPUT);
            BufferedReader lerFicheiro = new BufferedReader(ficheiro);

            String linha = lerFicheiro.readLine();
            while (linha != null) {
                String[] dados;

                if (linha.length() != 0) {
                    dados = linha.split(";");
                    for (int i = 0; i < dados.length; i++) {
                        dados[i] = dados[i].trim();
                    }
                    instancia(dados);
                }
                linha = lerFicheiro.readLine();
            }

            {
                ficheiro.close();
            }
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
                    e.getMessage());
        }

        System.out.println();
    }

    /**
     * Método que selecciona, de acordo com a primeira palavra de cada linha do
     * ficheiro de texto, o método adequado para instanciar os objetos com os
     * dados que recebe por paramêtro
     *
     * @param dados - dados dos objetos
     */
    private void instancia(String[] dados) {
        switch (dados[0].trim().toLowerCase()) {

            case "gestoreventos":
                criarGestorEventos(dados);
                break;

            case "evento":
                criarEvento(dados);
                break;

            case "fae":
                criarFAE(dados);
                break;

            case "participante":
                criarParticipante(dados);
                break;

            case "repparticipante":
                criarRepParticipante(dados);
                break;

            case "organizador":
                criarOrganizador(dados);
                break;

            case "candidatura":
                criarCandidatura(dados);
                break;

            default:
                break;
        }
    }

    /**
     * Método que recebe por parâmetro os dados (linha iniciada por "Utilizador"
     * e cria uma nova instancia de utilizador
     *
     * @param dados
     */
    private void criarGestorEventos(String[] dados) {

        GestorEventos gestorEventos = new GestorEventos();

        gestorEventos.setUsername(dados[1]);
        gestorEventos.setPassword(dados[2]);
        gestorEventos.setNome(dados[3]);
        gestorEventos.setEmail(dados[4]);

        gestores.add(gestorEventos);
//        this.centroEventos.adicionaUtilizador(gestorEventos);
    }

    /**
     * Método que recebe por parâmetro os dados (linha iniciada por "FAE" e cria
     * uma nova instancia de FAE
     *
     * @param dados
     */
    private void criarFAE(String[] dados) {
        FAE fae = new FAE(dados[1], dados[2], dados[3], dados[4], Integer.parseInt(dados[5]));
        faes.add(fae);

        
//        this.centroEventos.adicionaFAE(fae);
    }

    /**
     * Método que recebe por parâmetro os dados (linha iniciada por
     * "Participante" e cria uma nova instancia de Participante
     *
     * @param dados
     */
    private void criarParticipante(String[] dados) {
        Participante participante = new Participante(dados[1], Integer.parseInt(dados[2]));
        participantes.add(participante);
    }

    /**
     * Método que vai retornar um participante após percorrer o array de
     * participantes e procurar pelo nome
     *
     * @param nome do participante
     * @return
     */
    private Participante getParticipantePorNome(String nome) {
        for (Participante participante : participantes) {
            if (nome.equalsIgnoreCase(participante.getNome())) {
                return participante;
            }
        }
        return null;
    }

    /**
     * Método que recebe por parâmetro os dados (linha iniciada por
     * "RepParticipante" e cria uma nova instancia de RepParticipante
     *
     * @param dados
     */
    private void criarRepParticipante(String[] dados) {
        RepParticipante repParticipante = new RepParticipante(dados[1], dados[2], dados[3], dados[4], getParticipantePorNome(dados[5]));
        repsParticipantes.add(repParticipante);

    }

    /**
     * Método que recebe por parâmetro os dados (linha iniciada por
     * "Organizador" e cria uma nova instancia de Organizador
     *
     * @param dados
     */
    private void criarOrganizador(String[] dados) {
        Organizador organizador = new Organizador(dados[1], dados[2], dados[3], dados[4]);
        organizadores.add(organizador);
    }

    /**
     * Método que vai retornar um organizador após percorrer o array de
     * organizadores e procurar pelo nome
     *
     * @param nome do organizador
     * @return
     */
    private List<Organizador> getOrganizadoresPorNome(List<String> dados) {
        List<Organizador> lista = new ArrayList<>();
        for (Organizador organizador : organizadores) {
            for (String org : dados) {
                if (organizador.getNome().equalsIgnoreCase(org)) {
                    lista.add(organizador);
                }
            }

        }
        return lista;
    }

    /**
     * Método que recebe por parâmetro os dados (linha iniciada por "Evento" e
     * cria uma nova instancia de evento
     *
     * @param dados
     */
    private void criarEvento(String[] dados) {

        Evento evento = new Evento();

        evento.setTitulo(dados[1]);
        evento.setDescricao(dados[2]);
        evento.setLocal(dados[3]);

        String[] dataArray = dados[4].split("-");
        GregorianCalendar data = new GregorianCalendar(
                Integer.parseInt(dataArray[0]),
                Integer.parseInt(dataArray[1]),
                Integer.parseInt(dataArray[2]));
        evento.setDataInicio(data);

        dataArray = dados[5].split("-");
        data = new GregorianCalendar(
                Integer.parseInt(dataArray[0]),
                Integer.parseInt(dataArray[1]),
                Integer.parseInt(dataArray[2]));
        evento.setDataFim(data);

        dataArray = dados[6].split("-");
        data = new GregorianCalendar(
                Integer.parseInt(dataArray[0]),
                Integer.parseInt(dataArray[1]),
                Integer.parseInt(dataArray[2]));
        evento.setDataInicioCandidatura(data);

        dataArray = dados[7].split("-");
        data = new GregorianCalendar(
                Integer.parseInt(dataArray[0]),
                Integer.parseInt(dataArray[1]),
                Integer.parseInt(dataArray[2]));
        evento.setDataFimCandidatura(data);

        List<String> organizadoresEvento = new ArrayList<>();
        for (int i = 8; i < dados.length; i++) {
            organizadoresEvento.add(dados[i]);
        }
        evento.setListaOrganizadores(getOrganizadoresPorNome(organizadoresEvento));
        eventos.add(evento);

//        this.centroEventos.adicionaEvento(evento);
    }

    public List<Candidatura> getCandidaturasPorEvento (String tituloEvento) {
        List<Candidatura> myCandidaturas = new ArrayList<>();
        for (Candidatura candidatura : candidaturas) {
            if (candidatura.getEvento().getTitulo().equalsIgnoreCase(tituloEvento)) {
                myCandidaturas.add(candidatura);
            }
        }
        return myCandidaturas;
    }
    /**
     * Método que vai retornar um repParticipante após percorrer o array de
     * repParticipantes e procurar pelo nome
     *
     * @param nome do repParticipante
     * @return
     */
    private RepParticipante getRepParticipantePorUsername(String nome) {
        for (RepParticipante repParticipante : repsParticipantes) {
            if (nome.equalsIgnoreCase(repParticipante.getUsername())) {
                return repParticipante;
            }
        }
        return null;
    }

        /**
     * Método que vai retornar um evento após percorrer o array de
     * evento e procurar pelo titulo
     *
     * @param nome do repParticipante
     * @return
     */
    private Evento getEventoPorTitulo(String titulo) {
        for (Evento evento : eventos) {
            if (titulo.equalsIgnoreCase(evento.getTitulo())) {
                return evento;
            }
        }
        return null;
    }
    
    /**
     * Método que recebe por parâmetro uma linha de dados iniciada pela palavra
     * Candidatura, e cria uma instância de candidatura
     *
     * @param dados
     */
    private void criarCandidatura(String[] dados) {
        Candidatura candidatura = new Candidatura();
        Evento evento = getEventoPorTitulo(dados[2]);
        candidatura.setRepPart(getRepParticipantePorUsername(dados[1]));
        candidatura.setEvento(evento);
        evento.getCandidaturas().add(candidatura);
        candidaturas.add(candidatura);
    }

}
