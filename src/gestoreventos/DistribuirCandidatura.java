/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

/**
 *
 * @author joanagusmaoguedes
 */
public interface DistribuirCandidatura {

    /**
     * Distribui as candidaturas ao evento pelos FAEs escolhidos pelo
     * Organizador. - Se o nº de candidaturas for superior ao nº de FAEs
     * decisores: distrubui as candidaturas de forma igual entre os FAEs, e as
     * candidaturas sobrantes (resto) são distribuidas pelos primeiros FAEs do
     * array list. - Se o nº de candidaturas for inferior ao nº de FAEs
     * decisores: distrubui as candidaturas de forma igual pelos primeiros FAEs
     * do array list.
     *
     * @param evento
     */
    public abstract void Distribuicao(Evento evento);

    /**
     * Método que avalia se o prazo de submissão de candidaturas ao evento
     * terminou e, portanto, se a candidaturas ao evento pode ser distribuidas
     * pelos FAEs
     *
     *
     * @return true se as candidaturas puderem ser distrubuidas pelos FAEs,
     * false se não puderem.
     */
    public boolean isDistribuivel(Candidatura candidatura, Evento evento);

}
