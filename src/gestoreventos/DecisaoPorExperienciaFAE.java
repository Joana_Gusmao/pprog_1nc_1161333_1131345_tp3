/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class DecisaoPorExperienciaFAE extends Decisao {

    /**
     * Algoritmo de apoio à decisão que escolhe o(s) FAE(s)
     * considerando a sua experiência profissional na organização de
     * eventos. 
     *
     * @param evento - evento para o qual se pretende seleccionar os FAEs
     * @return lista dos FAEs escolhidos
     */
    @Override
    public List<FAE> EscolherFAEs(Evento evento, CentroEventos centroEventos, Object extra) {
        
        List<FAE> faesTotais = centroEventos.getFaes();
        List<FAE> faePorExperiencia = new ArrayList<>();
   
            for (FAE fae : faesTotais) {
                if (fae.getExperiencia() == (int) extra) {
                    faePorExperiencia.add(fae);
                }
            }
            
        
        return faePorExperiencia;
    }

}
