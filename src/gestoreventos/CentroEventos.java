package gestoreventos;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class CentroEventos {

    private String nome;
    private List<Evento> eventos = new ArrayList<>();
    private List<Evento> eventosSubmissiveis = new ArrayList<>();
    private List<Organizador> listaOrganizadores = new ArrayList<>();
    private List<RepParticipante> listaRepParticipantes = new ArrayList<>();
    private List<FAE> faes = new ArrayList<>();

//    private Evento evento;

    /**
     * Construtor completo da instancia CentroEventos
     *
     * @param nome - nome da instancia de Centro de Eventos
     * @param eventos - Array que contem todos os eventos do Centro de eventos
     * @param utilizadores - Array que contem todos os utilizadores do Centro de
     * eventos
     *
     */
    public CentroEventos(String nome, List<Evento> eventos, List<Organizador> listaOrganizadores,List<RepParticipante> listaRepParticipantes, List<FAE> faes ) {
        this.nome = nome;
        this.eventos = eventos;
        this.listaOrganizadores = listaOrganizadores;
        this.listaRepParticipantes = listaRepParticipantes;
        this.faes = faes;
    }

    /**
     * Construtor da instancia CentroEventos que recebe apenas o nome como
     * parâmetro
     *
     * @param nome - Atributo nome de uma instancia de Centro de Eventos
     */
    public CentroEventos(String nome) {
        this.nome = nome;
    }

    /**
     * Construtor da instancia CentroEventos que recebe o nome e lista de
     * eventos como parâmetros
     *
     * @param nome - nome da instancia de Centro de Eventos
     * @param eventos - Array que contem todos os eventos do Centro de eventos
     */
    public CentroEventos(String nome, List<Evento> eventos) {
        this.nome = nome;
        this.eventos = eventos;
    }

    /**
     * Construtor vazio da instância Centro de Eventos
     */
    public CentroEventos() {
        
    }

    /**
     * Método get/acesso para o atributo nome de um objecto de CentroEventos
     *
     * @return nome, atributo nome de CentroEventos
     */
    public String getNome() {
        return nome;
    }


    /**
     *
     * @return
     */
    public List<Evento> getEventos() {
        return eventos;
    }

    public List<RepParticipante> getListaRepParticipantes() {
        return listaRepParticipantes;
    }

    public List<FAE> getFaes() {
        return faes;
    }


    /**
     *
     * @return
     */
    public List<Organizador> getListaOrganizadores() {
        return listaOrganizadores;
    }

    /**
     *
     * @return
     */
    public List<Evento> getEventosSubmissiveis() {
        return eventosSubmissiveis;
    }



    /**
     * Método set/modificação para o atributo nome de um objecto de
     * CentroEventos
     *
     * @param nome - atributo nome de CentroEventos
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Método set/modificação para o ArrayList entidadesBomServico de um objecto
     * de CentroEventos
     *
     * @param eventos - arraylist de Evento
     */
    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }

    public void setEventosSubmissiveis(List<Evento> eventosSubmissiveis) {
        this.eventosSubmissiveis = eventosSubmissiveis;
    }

    public void setListaOrganizadores(List<Organizador> listaOrganizadores) {
        this.listaOrganizadores = listaOrganizadores;
    }

    public void setListaRepParticipantes(List<RepParticipante> listaRepParticipantes) {
        this.listaRepParticipantes = listaRepParticipantes;
    }

    public void setFaes(List<FAE> faes) {
        this.faes = faes;
    }



    /**
     * Lista todas as entidades do array list de eventos.
     *
     */
    public void listarEventos() {
        for (Evento evento : eventos) {
            System.out.println(evento.toString());
        }
    }

    /**
     *
     */
    public void ordenaEventos() {
        Collections.sort(eventos);
    }

    /**
     * Adiciona a instância de Evento recebida por parâmetro. Se a operação for
     * bem sucedida, deve retornar true, caso contrário deve retornar false.
     *
     * @param evento - Evento a adicionar
     * @return - true se adiciona
     */
    public boolean adicionaEvento(Evento evento) {
        return eventos.add(evento);
    }

    /**
     * Remove a primeira ocorrência da instância de Evento igual à recebida por
     * parâmetro, de acordo com o definido no Equals() da classe. Se a operação
     * for bem sucedida, deve retornar true. Caso contrário deve retornar false.
     *
     *
     * @param evento - Evento a remover
     * @return - true se encontra a instância a remover
     *
     */
    public boolean removeEvento(Evento evento) {
        return eventos.remove(evento);
    }

    /**
     * Adiciona ao array eventosApresentaveis, eventos pertencentes ao array
     * eventos cujo prazo de candidatura esteja a decorrer.
     *
     *
     * @param eventos - Lista de todos os Eventos do Centro de Eventos
     *
     */
    public List<Evento> criaEventosSubmissiveis() {

        for (Evento evento : eventos) {
            if (evento.isSumissivel()) {
                eventosSubmissiveis.add(evento);
            }
        }
        return eventosSubmissiveis;
    }

    /**
     * Método que retorna os títlos dos eventos aos quais se podem submeter
     * candidaturas
     *
     * @return string título do evento
     */
    public String apresentaInfoEventosSubmissiveis(List<Evento> eventosSubmissiveis) {
        String titulo = "";
        for (Evento evento : eventosSubmissiveis) {
            titulo = " \n-Evento: " + evento.getTitulo();
        }
        return titulo;
    }

    /**
     * selecciona pelo título um evento em fase de submissão
     *
     * @param titulo do evento
     * @return evento correspondente ao título
     */
    public Evento seleccionarEventoSubmissivel(String titulo) {
        criaEventosSubmissiveis();
        for (Evento evento : eventosSubmissiveis) {
            if (titulo.equalsIgnoreCase(evento.getTitulo().trim())) {
                Evento evento1 = evento;
                return evento1;
            }
        }
        return null;
    }

    /**
     * selecciona evento pelo título
     *
     * @param titulo do evento
     * @return evento correspondente ao título
     */
    public Evento seleccionarEventoPorTitulo(String titulo) {
        for (Evento evento : eventos) {
            if (titulo.trim().equalsIgnoreCase(evento.getTitulo().trim())) {
                Evento evento1 = evento;
                System.out.println("Evento"+evento);
                return evento1;
            }
        }
        return null;
    }

    /**
     * Selecciona uma candidatura pelo título do evento e username do representante do participante
     *
     * @param titulo do evento
     * @param username do representante do participante
     * @return candidatura correspondente ao evento e participante
     */
    public Candidatura seleccionarCandidaturaPorTituloEventoERepParticipante(String titulo, String username) {
        for (Evento evento : eventos) {
            if (titulo.equalsIgnoreCase(evento.getTitulo())) {
                List<Candidatura> candidaturasEvento = evento.getCandidaturas();
                for (Candidatura candidatura : candidaturasEvento) {
                    if (candidatura.getRepPart().getUsername().equalsIgnoreCase(username)) {
                        return candidatura;
                    }
                }
            }
        }
        return null;
    }

        /**
     * Método que vai retornar um repParticipante após percorrer o array de
     * repParticipantes e procurar pelo nome
     *
     * @param nome do repParticipante
     * @return
     */
    public RepParticipante getRepParticipantePorUsername(String nome) {
        for (RepParticipante repParticipante : listaRepParticipantes) {
            if (nome.equalsIgnoreCase(repParticipante.getUsername())) {
                return repParticipante;
            }
        }
        return null;
    }

        /**
     * Método que vai retornar um repParticipante após percorrer o array de
     * repParticipantes e procurar pelo nome
     *
     * @param nome do repParticipante
     * @return
     */
    public FAE getFAEPorUsername(String nome) {
        for (FAE fae : faes) {
            if (nome.equalsIgnoreCase(fae.getUsername())) {
                return fae;
            }
        }
        return null;
    }

//    /**
//     *Ordena o array de utilizadores alfabeticamente
//     */
//    public void ordenaUtilizadores() {
//        Collections.sort(utilizadores);
//    }





    public boolean adicionaFAE(FAE fae) {
        return faes.add(fae);
    }


//    /**
//     * percorre o array de Utilizadores e cria um novo array contendo apenas
//     * instâncias de FAE
//     *
//     * @return - lista dos utilizadores que são instâncias de FAE
//     */
//    public List<FAE> criarArrayFAEs() {
//
//        for (Utilizador utilizador : utilizadores) {
//            if (utilizador instanceof FAE) {
//                faes.add((FAE) utilizador);
//            }
//        }
//        return faes;
//    }

    /**
     * Método toString para imprimir o conteúdo do objecto criado em BomServiço
     *
     * @return - string com nome, telefone e nif
     */
    @Override
    public String toString() {
        return "- Centro de Eventos" + "\n  nome: " + nome + "\n  Lista de Eventos: " + eventos.toString();
    }

//    //ver método seleccionarEvento desta classe
//    public Evento getEvento(String dado) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//// ver método getRepresentanteParticipante desta classe
//
//    public Utilizador getUtilizador(String dado) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

}
