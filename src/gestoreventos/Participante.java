/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

import java.io.Serializable;
import java.util.Objects;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class Participante implements Serializable{
    
    private String nome;
    private int nif;
    
    /**
     * 
     * @param nome
     * @param nif 
     */
    public Participante (String nome, int nif){
        this.nome = nome;
        this.nif = nif;
    }
    /**
     * 
     */
    public Participante (){        
    }
    /**
     * 
     * @return 
     */
    public String getNome() {
        return nome;
    }
/**
 * 
 * @return 
 */
    public int getNif() {
        return nif;
    }
/**
 * 
 * @param nome 
 */
    public void setNome(String nome) {
        this.nome = nome;
    }
/**
 * 
 * @param nif 
 */
    public void setNif(int nif) {
        this.nif = nif;
    }
/**
 * 
 * @return 
 */
    @Override
    public String toString() {
        return "Participante{" + "nome=" + nome + ", nif=" + nif + '}';
    }
/**
 * 
 * @param obj
 * @return 
 */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Participante other = (Participante) obj;
        if (!this.nome.equalsIgnoreCase(other.nome) || this.nif != other.nif) {
            return false;
        }
        return true;
    }
    
    
    
}
