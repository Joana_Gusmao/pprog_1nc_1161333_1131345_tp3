/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

import java.io.Serializable;
import java.util.Objects;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class RepParticipante extends Utilizador implements /*Submissivel,*/ Serializable{

    private Participante participante;
/**
 * 
 * @param nome
 * @param username
 * @param password
 * @param email
 * @param participante 
 */
    public RepParticipante(String nome, String username, String password, String email, Participante participante) {
        super(nome, username, password, email);
        this.participante = participante;
    }
/**
 * 
 * @param nome
 * @param participante 
 */
    public RepParticipante(String nome, Participante participante){
        super(nome);
        this.participante=participante;
    }

/**
 * 
 * @param participante 
 */
    public RepParticipante(Participante participante) {
        super();
        this.participante = participante;
    }
/**
 * 
 * @return 
 */
    public Participante getParticipante() {
        return participante;
    }
/**
 * 
 * @param participante 
 */
    public void setParticipante(Participante participante) {
        this.participante = participante;
    }
/**
 * 
 * @return 
 */
    @Override
    public String toString() {
        return "RepresentanteDeParticipante - " + super.toString() + "participante:" + participante + '}';
    }

    /**
     * Método que compara RepredentanteDeParticipante com o objecto (obj) recebido por parâmetro
     *
     * @param obj o objecto que se compara com RepredentanteDeParticipante
     * @return true se o objecto recebido representar uma entidade equivalente
     * (incluindo o participante)a RepredentanteDeParticipante. Caso contrário
     * retorna false.
     */
    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        final RepParticipante other = (RepParticipante) obj;
        if (!Objects.equals(this.participante, other.participante)) {
            return false;
        }
        return true;
    }


    
}
