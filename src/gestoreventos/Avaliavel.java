/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public interface Avaliavel {

    /**
     * Método que avalia se a candidatura se encontra atribuida a um FAE e pode
     * ser por este avaliada
     *
     *
     * @return true se a candidatura tiver FAE atribuido ou false se não tiver
     */
    public boolean isAvaliavel();

    /**
     * Método que cria uma nova avaliação
     *
     * @return nova avaliação
     */
    public Avaliacao novaAvaliacao();

    /**
     * Método que adiciona a avaliação à candidatura avaliada
     *
     * @param avaliacao avaliacao a adicionar à candidatura
     * @return boolean - true se adicionada ou false se não adicionada
     */
    public boolean setAvaliacao(Avaliacao avaliacao);

    /**
     * Método que valida se a avaliação está completa
     *
     * @param avaliacao - avaliacao a validar
     */
    public boolean validaAvaliacao(Avaliacao avaliacao);

}
