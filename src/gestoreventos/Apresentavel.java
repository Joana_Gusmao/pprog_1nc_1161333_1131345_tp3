package gestoreventos;

import java.util.List;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public interface Apresentavel {

    public abstract List<Evento> removeEventoForaDePrazo();
}
