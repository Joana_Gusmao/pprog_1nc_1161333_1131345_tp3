package gestoreventos;

import java.io.Serializable;
import java.util.List;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class Candidatura implements Avaliavel,/*Submissivel*/ Serializable {

//    private Participante participante;
    private RepParticipante repPart;
    private Evento evento;
    private FAE faeDecisor;
    private Avaliacao avaliacao;

    /**
     *
     * @param participante
     * @param repPart
     */
    public Candidatura(/*Participante participante, */RepParticipante repPart, Evento evento) {
//        this.participante = participante;
        this.repPart = repPart;
        this.evento = evento;
    }

    /**
     *
     */
    public Candidatura() {
    }

//    /**
//     *
//     * @return
//     */
//    public Participante getParticipante() {
//        return this.participante;
//    }

    /**
     *
     * @return
     */
    public RepParticipante getRepPart() {
        return repPart;
    }

    /**
     *
     * @return
     */
    public Evento getEvento() {
        return evento;
    }


    /**
     *
     * @return
     */
    public FAE getFaeDecisor() {
        return faeDecisor;
    }

    /**
     *
     * @return
     */
    public Avaliacao getAvaliacao() {
        return avaliacao;
    }

//    /**
//     *
//     * @param participante
//     */
//    public void setParticipante(Participante participante) {
//        this.participante = participante;
//    }

    /**
     *
     * @param repPart
     */
    public void setRepPart(RepParticipante repPart) {
        this.repPart = repPart;
    }

    /**
     *
     * @param evento
     */
    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    /**
     *
     * @param faeDecisor
     */
    public void setFaeDecisor(FAE faeDecisor) {
        this.faeDecisor = faeDecisor;
    }


    @Override
    public String toString() {
        return "- Candidatura -" + repPart + "para o evento com o título: " + evento;
    }

    /**
     * Compara a candidatura com o objecto recebido
     *
     * @param obj o objecto que se compara com a candidatura
     * @return true se o objecto recebido representar uma candidatura
     * equivalente à candidatura. Caso contrário retorna false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Candidatura outraCandidatura = (Candidatura) obj;
        return /*this.participante.equals(outraCandidatura.participante) &&*/ this.repPart.equals(outraCandidatura.repPart);
    }

    /**
     * Método que avalia se a candidatura se encontra atribuida a um FAE e pode
     * ser por este avaliada
     *
     *
     * @return true se a candidatura tiver FAE atribuido ou false se não tiver
     */
    @Override
    public boolean isAvaliavel() {
        if (this.faeDecisor != null) {
            return true;
        }
        return false;
    }

    /**
     * Método que cria uma nova avaliação
     *
     * @return nova avaliação
     */
    @Override
    public Avaliacao novaAvaliacao() {
        return new Avaliacao();

    }


    /**
     * Método que adiciona a avaliação à candidatura avaliada
     *
     * @param avaliacao - avaliacao a adicionar à candidatura
     */
    @Override
    public boolean setAvaliacao(Avaliacao avaliacao) {
        if (validaAvaliacao(avaliacao)) {
            this.avaliacao = avaliacao;
            this.faeDecisor.diminuirCargaTrabalho();
            return true;
        }
        return false;
    }

    /**
     * Método que valida se a avaliação está completa
     *
     * @param avaliacao - avaliacao a validar
     */
    @Override
    public boolean validaAvaliacao(Avaliacao avaliacao) {
        if (avaliacao.getJustificação() != null && avaliacao.isAceite() == true || avaliacao.isAceite() == false) {
            return true;
        }
        return false;
    }
}
