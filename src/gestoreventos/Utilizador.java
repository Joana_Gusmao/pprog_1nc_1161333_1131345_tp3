/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public  class Utilizador implements Comparable<Utilizador>, Serializable{

    private String nome;
    private String username;
    private String password;
    private String email;

    private static final String NOME_POR_OMISSAO = "Sem_Nome";
    private static final String USERNAME_POR_OMISSAO = "Sem_Username";
    private static final String PASSWORD_POR_OMISSAO = "Sem_Password";
    private static final String EMAIL_POR_OMISSAO = "Sem_Email";

    public Utilizador(String nome, String username, String password, String email) {
        this.nome = nome;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public Utilizador(String nome) {
        this.nome = nome;
        this.username = USERNAME_POR_OMISSAO;
        this.password = PASSWORD_POR_OMISSAO;
        this.email = EMAIL_POR_OMISSAO;
    }

    public Utilizador() {
        this.nome = NOME_POR_OMISSAO;
        this.username = USERNAME_POR_OMISSAO;
        this.password = PASSWORD_POR_OMISSAO;
        this.email = EMAIL_POR_OMISSAO;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Métodos get para email, nome, userrname que retornam os mesmos
     */
    public String getEmail() {
        return this.email;
    }

    public String getNome() {
        return nome;
    }

    public String getUsername() {
        return username;
    }

    /**
     * Método toString para email, nome, username e email que retornam os
     * mesmos
     */
    @Override
    public String toString() {       
        return "Nome: " +this.nome + "\n; Username: " + this.username + "\n; Password: " + this.password + "\n; e-mail: " + this.email + "\n";
    }


    /**
     * compara o utilizador com o objecto recebido
     *
     * @param obj o objecto que se compara com o utilizador
     * @return true se o objecto recebido representar um utilizaodr equivalente ao
     * utilizador. Caso contrário retorna false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        final Utilizador other = (Utilizador) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return true;
    }
        
/**
 * 
 * @param o
 * @return 
 */
    //ordenar por classe e depois por nome de Utilizador
    @Override
    public int compareTo(Utilizador o) {
        String utilizadorA = this.getClass().getSimpleName();
        String utilizadorB = o.getClass().getSimpleName();
        int comparacao = utilizadorA.compareTo(utilizadorB);
        if (comparacao == 0) { //mesma classe, comparar por nome
            return this.getNome().compareToIgnoreCase(o.getNome());
        } else {
            return comparacao; //classes diferentes, usar comparação por classe
        }
        
    }

    
    
    

}
