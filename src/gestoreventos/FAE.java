/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class FAE extends Utilizador implements Comparable<Utilizador>, Serializable {

    //experiência é um inteiro, igual ao nº de eventos já organizados pelo FAE
    private int experiencia;
    private int contCarga = 0;
    private int contCargaTotal = 0;

    private List<Candidatura> candidaturasParaDecidir = new ArrayList<Candidatura>();

    /**
     *
     * @param nome
     * @param username
     * @param password
     * @param email
     * @param experiencia
     */
    public FAE(String nome, String username, String password, String email, int experiencia) {
        super(nome, username, password, email);
        this.experiencia = experiencia;
    }

    public FAE() {
        super();
    }

    /**
     *
     * @return
     */
    public int getExperiencia() {
        return experiencia;
    }

    /**
     *
     * @return
     */
    public int getContCarga() {
        return contCarga;
    }

    /**
     *
     * @return
     */
    public int getContCargaTotal() {
        return contCargaTotal;
    }

    /**
     *
     * @return
     */
    public List<Candidatura> getCandidaturasParaDecidir() {
        return candidaturasParaDecidir;
    }

    /**
     *
     * @param experiencia
     */
    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    /**
     *
     * @param contCarga
     */
    public void setContCarga(int contCarga) {
        this.contCarga = contCarga;
    }

    /**
     *
     * @param contCargaTotal
     */
    public void setContCargaTotal(int contCargaTotal) {
        this.contCargaTotal = contCargaTotal;
    }

    /**
     *Adiciona candidaturas sobre as quais o FAE tem que tomar uma decisão
     * @param candidaturasParaDecidir - candidatura a analisar
     */
    public boolean adicionaCandidaturaParaDecidir(Candidatura candidaturaParaDecidir ) {
        return candidaturasParaDecidir.add(candidaturaParaDecidir);
    }

    /**
     *
     * @return
     */
    public String toString() {
        return "- FAE - " + super.toString()+"\n"+getCandidaturasParaDecidir();
    }

    /**
     * Compara um FAE com outro Utilizador recebido por parâmetro. Se este for
     * um FAE, compara-os por ordem crescente de carga de trabalho na
     * organização de eventos.
     *
     * @param o - Objecto a ser comparado
     * @return o valor 0, se a sua carga de trabalho for igual; -1 ou 1 caso o a
     * sua carga de trabalho seja, respectivamente, maior ou menor à do FAE a
     * ser comparado.
     *
     */
    @Override
    public int compareTo(Utilizador o) throws ClassCastException {
        int comparacao;
        FAE fae = (FAE) o;
        if (this.contCarga > fae.contCarga) {
            comparacao = -1;
        } else if (this.contCarga < fae.contCarga) {
            comparacao = 1;
        } else {
            comparacao = 0;
        }
        return comparacao;
    }

    /**
     * Aumenta a carga de trabalho do faes quando lhe é atribuida uma revisão
     * de candidaturas para um evento
     */
    public void aumentarCargaTrabalho() {
            this.contCarga=++contCarga;
            this.contCargaTotal=++contCargaTotal;
        }
    

    /**
     * Aumenta a experiência do FAE na organização de eventos, até ao nível máximo de
     * 3, assim que este atinja os patamares definidos de nº de candidatura s por ele analisadas
     *
     */
    public void aumentarExperiencia() {
        int patamar1 = 50;
        int patamar2 = 150;

            if (this.getExperiencia() == 1 && this.contCargaTotal > patamar1) {
                this.setExperiencia(2);
            }
            if (this.getExperiencia() == 2 && this.contCargaTotal > patamar2) {
                this.setExperiencia(3);
            }
        }
    

    /**
     * Diminui a carga de trabalho do fae após tomar decisão sobre as candidaturas
     *
     * @param faesEscolhidos
     */
    public void diminuirCargaTrabalho() {
        this.contCarga = --contCarga;
    }
    
    
    /**
     * Visualização por extenso do patamar de experiência do fae
     */
    public void patamarDeExperiência (){
        int experiencia = this.getExperiencia();
        switch(experiencia){
            case 1:
                System.out.println("FAE pouco experiente");           
            case 2:
                System.out.println("FAE medianamente experiente");
            case 3:
                System.out.println("FAE muito experiente");
        }
    
    }


}
