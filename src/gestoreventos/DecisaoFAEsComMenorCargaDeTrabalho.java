/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class DecisaoFAEsComMenorCargaDeTrabalho extends Decisao {

    /**
     * Algoritmo de apoio à decisão que escolhe o(s) FAE(s)
     * com menor carga de trabalho no momento da sua selecção.
     *
     * @param evento - evento para o qual se pretende seleccionar os FAEs
     * @return lista dos FAEs escolhidos
     */
//    @Override
    public List<FAE> EscolherFAEs(Evento evento, CentroEventos centroEventos, Object extra) {
        List<FAE> faesTotais = centroEventos.getFaes();

        try {
            Collections.sort(faesTotais);
        } catch (ClassCastException e) {
            System.out.println("Utilizador não é um FAE");
        }
        
        List<FAE> faePorMenorCargaTrabalho = new ArrayList<>();
        
        int minCarga = faesTotais.get(0).getContCarga();
        for (FAE fae : faesTotais) {
            if (fae.getContCarga() == minCarga) {
                faePorMenorCargaTrabalho.add(fae);
            }
        }

        System.out.println(faePorMenorCargaTrabalho);
        return faePorMenorCargaTrabalho;
    }

}
