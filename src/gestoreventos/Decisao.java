/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

import java.util.List;

/**
 *
 * @author joanagusmaoguedes
 */
public abstract class Decisao {
    
    /**
     *
     * @param candidaturas
     * @param faes
     * @return
     */
    public abstract List<FAE> EscolherFAEs(Evento evento, CentroEventos centroEventos, Object extra);
    
}
