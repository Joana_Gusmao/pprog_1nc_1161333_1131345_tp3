package gestoreventos;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class Evento implements Comparable<Evento>, /*SubmeterCandidatura,*/ Serializable {

    private String titulo;
    private String descricao;
    private String local;
    private GregorianCalendar dataInicio;
    private GregorianCalendar dataFim;
    private GregorianCalendar dataInicioCandidatura;
    private GregorianCalendar dataFimCandidatura;
    private int nFAE;
    private List<Organizador> listaOrganizadores = new ArrayList<>();
//    private List<FAE> faesTotais;
    private List<FAE> faesEscolhidos = new ArrayList<>();
    private List<RepParticipante> listaRepParticipantes = new ArrayList<>();
//    private List<Participante> listaParticipantes = new ArrayList<>();
    private List<Candidatura> candidaturas = new ArrayList<>();
//    private List<Submissao> submissoes;

    private static final String TITULO_POR_DEFEITO = "sem título";
    private static final String DESCRIÇÃO_POR_DEFEITO = "sem descrição";
    private static final String LOCAL_POR_DEFEITO = "sem local";

    /**
     * Construtor da instancia Evento, que é criada pelo gestor de eventos,
     * recebendo como parâmetros o título do evento, texto descritivo, data de
     * início e de fim, local de realização e lista de organizadores
     * responsáveis
     *
     * @param titulo - título do evento
     * @param descricao - texto descritivo do evento
     * @param local - local de realização do evento
     * @param dataInicio - data de início do evento
     * @param dataFim - data de fim do evento
     * @param listaOrganizadores - lista de organizadores responsáveis pelo
     * evento
     */
    public Evento(String titulo, String descricao, String local, GregorianCalendar dataInicio, GregorianCalendar dataFim, GregorianCalendar dataInicioCandidatura, GregorianCalendar dataFimCandidatura, List<Organizador> listaOrganizadores) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.local = local;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.dataInicioCandidatura = dataInicioCandidatura;
        this.dataFimCandidatura = dataFimCandidatura;
        this.listaOrganizadores = listaOrganizadores; 
    }

    /**
     * Construtor vazio da instância Evento
     */
    public Evento() {
        this.titulo = TITULO_POR_DEFEITO;
        this.descricao = DESCRIÇÃO_POR_DEFEITO;
        this.local = LOCAL_POR_DEFEITO;
        this.listaOrganizadores = new ArrayList<>();
    }
    

    /**
     *
     * @return
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     *
     * @return
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     *
     * @return
     */
    public String getLocal() {
        return local;
    }

    /**
     *
     * @return
     */
    public GregorianCalendar getDataInicio() {
        return dataInicio;
    }

    /**
     *
     * @return
     */
    public GregorianCalendar getDataFim() {
        return dataFim;
    }

    /**
     *
     * @return
     */
    public GregorianCalendar getDataInicioCandidatura() {
        return dataInicioCandidatura;
    }

    /**
     *
     * @return
     */
    public GregorianCalendar getDataFimCandidatura() {
        return dataFimCandidatura;
    }

    /**
     *
     * @return
     */
    public List<Organizador> getOrganizadores() {
        return listaOrganizadores;
    }

    /**
     *
     * @return
     */
    public int getnFAE() {
        return nFAE;
    }

    /**
     *
     * @return
     */
    public List<FAE> getFaesEscolhidos() {
        return faesEscolhidos;
    }

    public void actualizarCargaTrabalhoEExperienciaFAEs() {
        for (FAE fae : faesEscolhidos) {
            fae.aumentarCargaTrabalho();
            fae.aumentarExperiencia();
        }
    }

    /**
     *
     * @return
     */
    public List<RepParticipante> getRepParticipantes() {
        return listaRepParticipantes;
    }

//    /**
//     *
//     * @return
//     */
//    public List<Participante> getParticipantes() {
//        return listaParticipantes;
//    }

    /**
     *
     * @return
     */
    public List<Organizador> getListaOrganizadores() {
        return listaOrganizadores;
    }

    /**
     *
     * @return
     */
    public List<RepParticipante> getListaRepParticipantes() {
        return listaRepParticipantes;
    }

//    /**
//     *
//     * @return
//     */
//    public List<Participante> getListaParticipantes() {
//        return listaParticipantes;
//    }

    /**
     *
     * @return
     */
    public List<Candidatura> getCandidaturas() {
        return candidaturas;
    }

    /**
     *
     * @return
     */
    public String getTITULO_POR_DEFEITO() {
        return TITULO_POR_DEFEITO;
    }

    /**
     *
     * @return
     */
    public String getDESCRIÇÃO_POR_DEFEITO() {
        return DESCRIÇÃO_POR_DEFEITO;
    }

    /**
     *
     * @return
     */
    public String getLOCAL_POR_DEFEITO() {
        return LOCAL_POR_DEFEITO;
    }

    /**
     *
     * @param titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     *
     * @param descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     *
     * @param local
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     *
     * @param dataInicio
     */
    public void setDataInicio(GregorianCalendar dataInicio) {
        this.dataInicio = dataInicio;
    }

    /**
     *
     * @param dataFim
     */
    public void setDataFim(GregorianCalendar dataFim) {
        this.dataFim = dataFim;
    }

    /**
     *
     * @param dataInicioCandidatura
     */
    public void setDataInicioCandidatura(GregorianCalendar dataInicioCandidatura) {
        this.dataInicioCandidatura = dataInicioCandidatura;
    }

    /**
     *
     * @param dataFimCandidatura
     */
    public void setDataFimCandidatura(GregorianCalendar dataFimCandidatura) {
        this.dataFimCandidatura = dataFimCandidatura;
    }

    /**
     *
     * @param nFAE
     */
    public void setnFAE(int nFAE) {
        this.nFAE = nFAE;
    }

    /**
     *
     * @param listaOrganizadores
     */
    public void setOrganizadores(List<Organizador> listaOrganizadores) {
        this.listaOrganizadores = listaOrganizadores;
    }

    /**
     *
     * @param faes
     */
    public void setFaesEscolhidos(List<FAE> faes) {
        System.out.println(faes);
        this.faesEscolhidos = faes;
    }

    /**
     *
     * @param listaOrganizadores
     */
    public void setListaOrganizadores(List<Organizador> listaOrganizadores) {
        this.listaOrganizadores = listaOrganizadores;
    }

    /**
     *
     * @param listaRepParticipantes
     */
    public void setListaRepParticipantes(List<RepParticipante> listaRepParticipantes) {
        this.listaRepParticipantes = listaRepParticipantes;
    }

//    /**
//     *
//     * @param listaParticipantes
//     */
//    public void setListaParticipantes(List<Participante> listaParticipantes) {
//        this.listaParticipantes = listaParticipantes;
//    }

    /**
     *
     * @param candidaturas
     */
    public void setCandidaturas(List<Candidatura> candidaturas) {
        this.candidaturas = candidaturas;
    }

    /**
     * Lista todos os Organizadores do array list de Organizadores
     */
    public void listarOrganizadores() {
        for (Organizador organizador : listaOrganizadores) {
            System.out.println(organizador.toString());
        }
    }

    /**
     * Lista todos os RepParticipantes do array list de RepParticipantes
     */
    public void listarRepParticipantes() {
        for (RepParticipante repParticipante : listaRepParticipantes) {
            System.out.println(repParticipante.toString());
        }
    }
//
//    /**
//     * Lista todos os Participantes do array list de Participantes
//     */
//    public void listarParticipantes() {
//        for (Participante participante : listaParticipantes) {
//            System.out.println(participante.toString());
//        }
//    }
//
//    public boolean adicionaParticipante(Participante participante) {
//        return listaParticipantes.add(participante);
//    }

    /**
     * Adiciona a instância de Organizador recebida por parâmetro. Se a operação
     * for bem sucedida, deve retornar true, caso contrário deve retornar false.
     *
     * @param organizador - Organizador a adicionar
     * @return - true se adiciona
     */
    
    public boolean adicionaOrganizador(Organizador organizador) {
        return listaOrganizadores.add(organizador);
    }

    public void criarCandidatura(/*Participante participante,*/ RepParticipante representante, Evento evento) {
        candidaturas.add(new Candidatura(/*participante,*/ representante, evento));
    }

    /**
     * Adiciona a instância de Candidatura recebida por parâmetro. Se a operação
     * for bem sucedida, deve retornar true, caso contrário deve retornar false.
     *
     * @param candidatura - Candidatura a adicionar
     * @return - true se adiciona
     */
    public boolean adicionaCandidatura(Candidatura candidatura) {
        return candidaturas.add(candidatura);
    }

    //isto está bem? o compareTo que vai ser usado é o de Utilizadores. 
    // fazer uma validação primeiro? if (organizador instaceOf Organizador)?
    /**
     *
     */
    public void ordenaOrganizadores() {
        Collections.sort(listaOrganizadores);
    }

    /**
     * Lista todos os FAEs do array list de FAEs
     */
    public void listarFAEs() {
        for (FAE fae : faesEscolhidos) {
            System.out.println(fae.toString());
        }
    }

//    public boolean adicionaFAE(Utilizador utilizador) {
//        return .add(utilizador);
//    }
    /**
     * Adiciona a instância de FAE recebida por parâmetro. Se a operação for bem
     * sucedida, deve retornar true, caso contrário deve retornar false.
     *
     * @param fae - FAE a adicionar
     * @return - true se adiciona
     */
    public boolean adicionaFAE(FAE fae) {
        return faesEscolhidos.add(fae);
    }

    /**
     * Remove a primeira ocorrência da instância de FAE igual à recebida por
     * parâmetro, de acordo com o definido no Equals() da classe. Se a operação
     * for bem sucedida, deve retornar true. Caso contrário deve retornar false.
     *
     *
     * @param fae - FAE a remover
     * @return - true se encontra a instância a remover
     *
     */
    public boolean removeFAE(FAE fae) {
        return faesEscolhidos.remove(fae);
    }

    /**
     * Avalia se o evento está em periodo de aceitação de candidaturas
     *
     * @return true se aceita, false se não aceita
     */
    public boolean isSumissivel() {
        GregorianCalendar dataActual = new GregorianCalendar();
        return dataActual.equals(this.getDataInicioCandidatura()) || 
                (dataActual.after(this.getDataInicioCandidatura()) && dataActual.before(this.getDataFimCandidatura()));
    }

    /**
     * Distribui as candidaturas ao evento pelos FAEs escolhidos pelo
     * Organizador, e actuliza a experiência e carga de trabalho dos FAEs
     * consoante o nº de candidaturas que têm qua analisar.
     */
    public void distribuiCandidaturasEActualizaCargaTrabalhoEExperiencia() {
        int totalFaes = faesEscolhidos.size();
        for (int i = 0; i < candidaturas.size(); i++) {
            Candidatura candidatura = candidaturas.get(i);
            FAE fae = faesEscolhidos.get(i % totalFaes);
            fae.getCandidaturasParaDecidir()
                    .add(candidatura);
            fae.aumentarCargaTrabalho();
            fae.aumentarExperiencia();
            candidatura.setFaeDecisor(fae);
        }
    }

    /**
     *
     * @return
     */
    @Override
    //queremos a lista de organizadores no toString?
    //acho que a lista de FAEs e de candidaturas tb não deve estar no toString
    public String toString() {
        return "- Evento - " + "titulo: " + titulo +"\n";//+ "; descricao: " + descricao + "; local: " + local + "; dataInicio: " + dataInicio + "; dataFim: " + dataFim; //+ "; organizador:" + getOrganizadores() + "; FAE: " + getFaes()+ "; candidaturas: " + getCandidaturas();
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj
    ) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        final Evento other = (Evento) obj;
        if (!Objects.equals(this.titulo, other.titulo)) {
            return false;
        }
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        if (!Objects.equals(this.local, other.local)) {
            return false;
        }
        if (!Objects.equals(this.dataInicio, other.dataInicio)) {
            return false;
        }
        if (!Objects.equals(this.dataFim, other.dataFim)) {
            return false;
        }

        return true;
    }

    /**
     * Compara um Evento com outro Evento recebido por parâmetro, por ordem
     * temporal do prazo final de submossão de candidaturas.
     *
     * @param ev - Evento a ser comparado
     * @return - o valor -1, 0 ou 1, respectivamente, se a Data de Fim de
     * submissão de candidaturas ao evento mais recente, igual ou mais tardia do
     * que a do evento a ser comparado
     */
    @Override
    public int compareTo(Evento ev) {
        if (this.getDataFim().after(ev.dataFim)) {
            return 1;
        } else if (!this.getDataFim().before(ev.dataFim)) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * Método que retorna um Representante de participante procurando o mesmo
     * por nome na lista de representante participante
     *
     * @param dado
     * @return
     */
    public RepParticipante getRepParticipantePorNome(String dado) {
        for (RepParticipante repparticipante : listaRepParticipantes) {
            if (repparticipante.getNome().equalsIgnoreCase(dado)) {
                return repparticipante;
            }
        }
        return null;
    }
}

