/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class Avaliacao {

    private boolean aceite;
    private String justificação;

    /**
     * Construtor com parâmetros que instâcia uma nova avaliação de uma
     * candidatura
     *
     * @param aceite - true se aceite, false se rejeitada
     * @param justificação - string com texto justificativo de
     * aceitação/rejeição
     * @param candidatura - candidatura a avaliar
     */
    public Avaliacao(boolean aceite, String justificação, Candidatura candidatura) {
        this.aceite = aceite;
        this.justificação = justificação;

    }

    /**
     * Constroi uma instância de avaliação
     */
    public Avaliacao() {
    }

    /**
     * retorna a decisão de aceitação
     *
     * @return true se aceite, false se rejeitada
     */
    public boolean isAceite() {
        return aceite;
    }

    /**
     * retorna o texto justificativo da decisão de aceitação/rejeição
     *
     * @return texto justificativo
     */
    public String getJustificação() {
        return justificação;
    }

    /**
     * Modifica a decisão de aceitação
     *
     * @param aceite - true se aceite, false se rejeitada
     */
    public void setAceite(boolean aceite) {
        this.aceite = aceite;
    }

    /**
     * Modifica o texto justificativo da decisão de aceitação/rejeição
     *
     * @param justificação - texto justificativo da decisão
     */
    public void setJustificação(String justificação) {
        this.justificação = justificação;
    }

    /**
     * Retorna uma string com os dados da candidatura
     *
     * @return dados da candidatura a avaliar
     */
    @Override
    public String toString() {
        return "Aceite? " + this.isAceite() + "Justificação: " + this.getJustificação();
    }

    /**
     * Método que cria uma nova candidatura e retorna-a
     *
     * @return nova candidatura
     */
    public Candidatura novaCandidatura() {
        return new Candidatura();
    }

}
