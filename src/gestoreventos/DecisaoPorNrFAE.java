/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class DecisaoPorNrFAE extends Decisao /*implements DecisaoMecanismoApoio*/ {

    /**
     * Algoritmo de apoio à decisão que escolhe de forma aleatória, o nº de FAEs
     * pretendidos
     *
     * @param candidaturas - lista de candidaturas ao evento que se vão
     * distribuir pelo(s) FAE(s)
     * @param faes - lista de todos os FAEs registados como utilizadores
     * @return lista dos FAEs escolhidos
     */
//    @Override
    public List<FAE> EscolherFAEs(Evento evento, CentroEventos centroEventos, Object extra) {
        
        int nFAEs = evento.getnFAE();
        List<FAE> faesTotais = centroEventos.getFaes();
        List<FAE> faePorNrFAE = new ArrayList<>();

        Random numAleatorio = new Random();
                      
        do {
            int i = numAleatorio.nextInt(faesTotais.size());
            FAE fae = faesTotais.get(i);
            faePorNrFAE.add(fae);
        } while (faePorNrFAE.size()<nFAEs);
        
        return faePorNrFAE;
    }

}
