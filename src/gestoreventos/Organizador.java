/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestoreventos;


import java.io.Serializable;


/**
 * PPROG 2016/2017 Trabalho Prático nº3 - TP3
 *
 * @author 1161333 - Joana Gusmao Guedes
 * @author 1131345 - Jorge Mota
 *
 * @since 27/05/2017
 */
public class Organizador extends Utilizador implements Serializable {

    /**
     *
     * @param nome
     * @param username
     * @param password
     * @param email
     */
    public Organizador(String nome, String username, String password, String email) {
        super(nome, username, password, email);
    }

    /**
     *
     */
    public Organizador() {
        super();
    }

    @Override
    public String toString() {
        return "- Organizador - " + super.toString();
    }
}





    


